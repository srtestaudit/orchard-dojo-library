# Complementary topics in module development



- Writing unit tests with help of the [Lombiq Testing Toolbox](https://github.com/Lombiq/Testing-Toolbox) and UI tests with the help of [Lombiq UI Testing Toolbox](https://github.com/Lombiq/UI-Testing-Toolbox).
- Middlewares
- Integrating with client-side build pipelines using [Lombiq NPM MSBuild Targets](https://github.com/Lombiq/NPM-Targets) and [Lombiq Gulp Extensions](https://github.com/Lombiq/Gulp-Extensions), or using Vue with the [Lombiq Vue.js module](https://github.com/Lombiq/Orchard-Vue.js).

Time requirement: 2h 0m

Dependencies: [Basic techniques in module development](BasicTechniquesInModuleDevelopment)

Parent topic: [Module development and APIs](./)